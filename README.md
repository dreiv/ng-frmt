# NgFrmt

This project is a path to configure your angular cli project to with linting as
well as formatting capabilities.

## Setting up ESLint to work with TypeScript

First, install all the required dev dependencies:

```bash
npm i -D eslint @typescript-eslint/parser @typescript-eslint/eslint-plugin

npm i -D prettier eslint-config-prettier eslint-plugin-prettier
```
* [`eslint`](https://www.npmjs.com/package/eslint): The core ESLint linting library
* [`@typescript-eslint/parser`](https://www.npmjs.com/package/@typescript-eslint/parser): The parser that will allow ESLint to lint TypeScript code
* [`@typescript-eslint/eslint-plugin`](https://www.npmjs.com/package/@typescript-eslint/eslint-plugin): A plugin that contains a bunch of ESLint rules that are TypeScript specific
* [`prettier`](https://www.npmjs.com/package/prettier): The core prettier library
* [`eslint-config-prettier`](https://www.npmjs.com/package/eslint-config-prettier): Disables ESLint rules that might conflict with prettier
* [`eslint-plugin-prettier`](https://www.npmjs.com/package/eslint-plugin-prettier): Runs prettier as an ESLint rule

Next, add an `.eslintrc.json` configuration file in the root project directory. Here is a sample configuration for a TypeScript project:
```json
{
  "parser": "@typescript-eslint/parser", // Specifies the ESLint parser
  "extends": [
    "plugin:@typescript-eslint/recommended", // Uses the recommended rules from the @typescript-eslint/eslint-plugin
    "prettier/@typescript-eslint", // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
    "plugin:prettier/recommended" // Enables eslint-plugin-prettier and eslint-config-prettier. This will display prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
  ],
  "parserOptions": {
    "ecmaVersion": 2020, // Allows for the parsing of modern ECMAScript features
    "sourceType": "module" // Allows for the use of imports
  },
  "rules": {
    // Place to specify ESLint rules. Can be used to overwrite rules specified from the extended configs
  }
}
```

Ultimately it's up to you to decide what rules you would like to extend from and which ones to use within the `rules` object in your `.eslintrc.json` file.

In order to configure prettier, a `.prettierrc.json` file is required at the root project directory. Here is a sample `.prettierrc.json` file:
```json
{
  "singleQuote": true,
  "trailingComma": "none",
  "endOfLine": "auto"
}
```

Also add a `.eslintignore` file with files we don't want to format:
```
package.json
package-lock.json
dist
```

## Automatically Fix Code in VS Code
here is the config required in the `settings.json` file in VS Code to get automatic fixing whenever saving a file:
```json
{
  "eslint.validate": [
    "javascript",
    "typescript",
    "json"
  ],
  "editor.defaultFormatter": "dbaeumer.vscode-eslint",
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  "editor.formatOnSave": false
}
```

## Run ESLint with the CLI
A useful command to add to the [`package.json` scripts](https://docs.npmjs.com/misc/scripts) is a `lint` command that will run the TypeScript compiler and the ESLint linter across all project files to make sure the code adheres to the compiler settings and formatting/style rules.
```json
{
  "scripts": {
    "lint": "tsc --noEmit && eslint . --ext js,ts,json --quiet --fix"
  }
}
```
The above script can be run from the command line using `npm run lint`. This command will first run the TypeScript compiler and report any TypeScript compiler errors. If there are no TypeScript errors, it will then run ESLint through all the .ts files. Any ESLint errors that can be automatically fixed will be fixed with this command, but any other errors will be printed out in the command line.
- [TypeScript CLI Options](https://www.typescriptlang.org/docs/handbook/compiler-options.html)
- [ESLint CLI Options](https://eslint.org/docs/user-guide/command-line-interface)
- [ESLint plugins and configs](https://github.com/dustinspecker/awesome-eslint)

## Plugins
Installing some usefull plugins:
```bash
npm i -D eslint-plugin-json-format eslint-plugin-no-constructor-bind eslint-plugin-promise eslint-plugin-simple-import-sort eslint-plugin-sonarjs eslint-plugin-switch-case eslint-plugin-unicorn
```
and update your `.eslintrc.json`:
```json
{
  "parser": "@typescript-eslint/parser", // Specifies the ESLint parser
  "plugins": [
    "json-format", // Format, auto-fix, and sort your json files
    "no-constructor-bind", // Prefer class properties to equivalent setup steps taken in a class' constructor method
    "promise", // Enforces best practices for JavaScript promises
    "simple-import-sort", // Enforces import sorting
    "sonarjs", // SonarJS rules for ESLint to detect bugs and suspicious patterns in your code
    "switch-case" // Switch-case-specific linting rules for ESLint
  ],
  "extends": [
    "plugin:@typescript-eslint/recommended", // Uses the recommended rules from the @typescript-eslint/eslint-plugin
    "prettier/@typescript-eslint", // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier,
    "plugin:promise/recommended",
    "plugin:sonarjs/recommended",
    "plugin:switch-case/recommended",
    "plugin:unicorn/recommended",
    "plugin:prettier/recommended" // Enables eslint-plugin-prettier and eslint-config-prettier. This will display prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array
  ],
  "parserOptions": {
    "ecmaVersion": 2020, // Allows for the parsing of modern ECMAScript features
    "sourceType": "module" // Allows for the use of imports
  },
  "rules": {
    // Place to specify ESLint rules. Can be used to overwrite rules specified from the extended configs
    "prettier/prettier": "error",
    "no-constructor-bind/no-constructor-bind": "error",
    "no-constructor-bind/no-constructor-state": "error",
    "simple-import-sort/sort": "error",
    "sort-imports": "off",
    "switch-case/newline-between-switch-case": [
      "error",
      "always",
      { "fallthrough": "never" }
    ],
    "unicorn/explicit-length-check": 0
  }
}

```
